<?php

namespace App\Http\Controllers\API;

use App\Course;
use App\Exports\CoursesExport;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessCourseFactory;
use App\RegisteredCourse;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Facades\Excel;

class CourseController extends Controller
{
    // Get All Courses
    public function getCourses()
    {
        return Course::latest()->get();
    }

    // Download or Export Courses
    public function download()
    {
        return Excel::download(new CoursesExport, $this->getExportName());
    }

    // Courses File Export Name Format
    public function getExportName()
    {
        return "Courses_Export_" . date('d_m_Y') . '' . time() . '.xlsx';
    }

    // Course Factory
    public function factory()
    {
        \dispatch(new ProcessCourseFactory());
        return \response()->json([
            "status" => true,
            "message" => "Courses Populated",
        ]);
    }

    // Register Course
    public function regiserCourses(HttpRequest $request)
    {

        $courses = $request->courses;

        foreach ($courses as $course) {
            RegisteredCourse::create([
                'course_id' => $course,
                'user_id' => auth()->id()
            ]);
        }

        return response()->json([
            "status" => true,
            "messages" => "Courses Registered Successfully",
        ]);
    }
}
