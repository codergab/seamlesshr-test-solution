<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\RegisteredCourse;
use Illuminate\Http\Request;

class RegisteredCourseController extends Controller
{
    public function getUserCourses()
    {

        return RegisteredCourse::with('course')->where('user_id', auth()->id())->latest()->get();
    }
}
