<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{


    // Create User
    public function createUser(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed'
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => false,
                "errors" => $validator->errors()
            ], 400);
        }

        try {
            $user = User::create($request->all());
            return response()->json([
                "status" => true,
                "message" => "User Created Successfully",
                "data" => $user
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "status" => false,
                "error" => $th->getMessage()
            ], 500);
        }
    }

    // Authenticate User
    public function authenticate(Request $request)
    {
        $userDetails = request(['email', 'password']);

        // try to auth and get the token using api authentication
        if (!$token = JWTAuth::attempt($userDetails)) {
            // if the credentials are wrong we send an unauthorized error in json format
            return response()->json([
                "status" => false,
                "error" => 'Invalid Credentials'
            ], 401);
        }

        return response()->json([
            "status" => true,
            "message" => "User Authenticated",
            'token' => $token, // time to expiration
        ]);
    }

    // Get logged in user
    public function getUser()
    {
        return response()->json([
            "status" => true,
            'message' => "User Fetched Successfully",
            "data" => auth('api')->user()
        ]);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json([
            'message' => "Logged Out Successfully"
        ]);
    }
}
