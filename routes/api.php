<?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('users', 'API\AuthController@createUser');
Route::post('users/authenticate', 'API\AuthController@authenticate');


Route::middleware('jwt.auth')->group(function () {
    Route::get('users', 'API\AuthController@getUser');
    // Courses
    Route::prefix('courses')->group(function () {
        // Get All Courses
        Route::get('/', 'API\CourseController@getCourses');
        // Create Courses Using Factory
        Route::get('/factory', 'API\CourseController@factory');
        // User Registers Courses
        Route::post('user/register', 'API\CourseController@regiserCourses');
        // User Registered Courses
        Route::get('user/registered', 'API\RegisteredCourseController@getUserCourses');
    });
});

// Download Courses
Route::get('/courses/download', 'API\CourseController@download');
