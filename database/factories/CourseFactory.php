<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        "text" => $faker->sentence,
        "title" => $faker->words(3, true),
        "description" => $faker->realText(200),
        "instructor" => $faker->name,
        "created_at" => now(),
        "updated_at" => now()
    ];
});
